resource "aws_route53_zone" "primary" {
  name = "veselovskyi-v.tk"
}

resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "veselovskyi-v.tk"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.front_server.public_ip]
}
