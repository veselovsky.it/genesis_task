terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}

variable "pub_key" {
  default = "/Users/vlades233/.ssh/id_rsa_new.pub"
}
variable "pvt_key" {
  default = "/Users/vlades233/.ssh/id_rsa_new"
}

data "aws_region" "current" {}
