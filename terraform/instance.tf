resource "aws_instance" "front_server" {
  ami = "ami-0da1467ee72061020"
  #instance_type = "t2.small"
  instance_type               = "t2.micro" # сделал микро так как фри тир, меняется эта строка на строоку выше и будет так как вы просили в тестовом
  subnet_id                   = aws_subnet.main.id
  vpc_security_group_ids      = [aws_security_group.allow_http.id, aws_security_group.allow_ssh.id, aws_security_group.allow_https.id]
  key_name                    = "test_task"
  associate_public_ip_address = "true"

  tags = {
    Name      = "front_server",
    Terraform = "true"
  }
  provisioner "remote-exec" {
    inline = ["sudo apt update", "sudo apt install python3 -y", "echo python3_updated!"]

    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "admin"
      private_key = file(var.pvt_key)
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u admin -i '${self.public_ip},' --private-key ${var.pvt_key} -e 'pub_key=${var.pub_key}' ../ansible/main.yml"
  }
}
